NULL =

PROJECT = hirsute

SHEETS = \
	baldboss \
	boss \
	cabinet \
	james \
	kachiko \
	rae \
	speech \
	spy \
	stage \
	$(NULL)

TEXTURES := $(patsubst %,res/%.png,$(SHEETS))
ATLAS := res/tiles.atlas

FONT_TEXTURE = res/AmaticSC.png
FONT_FNT := $(patsubst %.png,%.fnt,$(FONT_TEXTURE))

RESOURCES = \
	$(ATLAS) \
	$(FONT_TEXTURE) \
	$(FONT_FNT) \
	$(TEXTURES) \
	$(NULL)

HAXE_SRC := $(wildcard src/*.hx)

HAXE = haxe
HAXEPORT = 6000
JSBIN = bin/$(PROJECT).js
HLBIN = bin/$(PROJECT).hl

ZIP = zip
ZIPFLAGS = -9 -j
ZIPBIN = bin/$(PROJECT).zip

define haxe_build
$(HAXE) --connect $(HAXEPORT) $(1) || ( [ $$? -eq 2 ] && $(HAXE) $(1) )
endef

$(HLBIN): $(PROJECT).hl.hxml $(PROJECT).hxml $(HAXE_SRC) | $(RESOURCES)
	$(call haxe_build,--debug --dce no --hl $@ $<)

$(JSBIN): $(PROJECT).js.hxml $(PROJECT).hxml $(HAXE_SRC) | $(RESOURCES)
	$(call haxe_build,--js $@ $<)

$(ZIPBIN): $(JSBIN) bin/index.html
	$(ZIP) $(ZIPFLAGS) $@ $^

$(ATLAS): $(TEXTURES)
	extractatlas $^ $@

res/%.png: assets/%.svg
	IDS=$$(xml select --text --template --match '/svg:svg/svg:g/svg:*' --value-of '@id' --output ';' $<) && \
		inkscape --export-id-only --export-type=png --export-id="$${IDS::${#IDS}-1}" $<
	packtextures assets/$*_*.png $@
	rm assets/$*_*.png

res/%.png res/%.fnt: fonts.json assets/%.ttf
	fontgen $<

all: $(HLBIN) dist

dist: $(ZIPBIN)

run: $(HLBIN)
	hl --hot-reload $<

watch:
	while true; do $(MAKE); inotifywait --recursive --event modify assets src *.hxml Makefile; sleep 0.25; done

listen:
	haxe --server-listen $(HAXEPORT)

clean:
	rm -fr $(HLBIN) $(JSBIN) $(ZIPBIN) $(RESOURCES)

.PHONY: all dist run watch listen clean
