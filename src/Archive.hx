import h2d.Flow;
import hxd.Res;
import Coroutine.Result;
import Coroutine.Co;

class Archive extends Scene {
  public function new() {
    super();

    final background = new AtlasBitmap("stage_archive", this);

    door = new AtlasAnim("stage_closed_door", 1, this, FlowAlign.Left, FlowAlign.Top);
    door.setPosition(51, 217);

    james = new James(this);

    cabinet = new Actor("cabinet", this);
    cabinet.setPosition(697, 577);
    cabinet.walkToPoint = -150;

    spy = new Actor("spy", this);
    spy.setPosition(450, 560);
    spy.walkToPoint = -150;

    addHotspot(cabinet, "Cabinet");
    whenClick(() -> {
	Co.fn({
	    james.walkTo(cabinet);
	    james.say("Here it is.");
	    james.say("Mocking me, with his one hair.");
	    james.say("I WILL find you, Oiseau!");
	  });
      });

    addHotspot(Std.int(door.x), Std.int(door.y), 85, 345, "To office");
    whenClick(() -> {
	Co.fn({
	    james.walkTo(190);
	    james.faceLeft();
	    fadeOut();
	    play(Res.office, false);
	    playAndWait(Res.open);
	    playAndWait(Res.close);
	    shiftScene(new Office());
	  });
      });

    addHotspot(310, 0, 180, 225, "Flourescent lighting");
    whenClick(() -> Co.fn({
	  james.say("They had to reduce the lighting’s intensity.");
	  james.say("People complained that were blinded when the light bounced off my head.");
	  wait(.5);
	  james.say("Bunch of whiners.");
	}));

    addHotspot(430, 220, 210, 265, "Archives");
    whenClick(() -> Co.fn({
	  james.say("Everything in it’s place. Neat.");
	}));
    
    addHotspot(spy, "Leo");
    whenClick(() -> {
	if (state.talked_boss > 0 && !state.commented_buns) {
	  state.commented_buns = true;
	  Co.fn({
	      james.say("He *does* have nice buns.");
	      wait(1);
	      james.say("The ones made with hair, that is.");
	    });
	} else if (state.quest_for_quill && !state.has_quill) {
	  state.has_quill = true;
	  Co.fn({
	      walkToAndFace(spy);
	      james.say("I’m sorry to ask this, but... do you know what a “digital quill” is?");
	      spy.say("Of course--it’s used to digitally sign documents.");
	      spy.say("It has a special digital ink for such purposes.");
	      spy.say("The signee then just has to make a photo with a digital camera.");
	      spy.say("Attach the image inside a Word document, with a scan of the original document.");
	      spy.say("And print the result--a digitally signed document!");
	      wait(1);
	      james.say("I...");
	      james.say("You know what? Not my fight.");
	      james.say("Could you fetch me one of these, please?");
	      spy.say("Right away, sir.");
	    spy.walkTo(190);
	    spy.faceLeft();
	    fadeOut();
	    play(Res.office, false);
	    playAndWait(Res.open);
	    playAndWait(Res.close);
	    stop(Res.office);
	    wait(1);
	    play(Res.office, false);
	    playAndWait(Res.open);
	    playAndWait(Res.close);
	    stop(Res.office);
	    spy.faceRight();
	    fadeIn();
	    spy.walkTo(450);
	    spy.faceTowards(james);
	    spy.say("Here your are, sir.");
	    james.say("...thanks");
	    });
	} else {
	  switch(state.talked_leo++) {
	  case 0:
	    Co.fn({
		walkToAndFace(spy);
		james.say("So, Leo....");
		james.say("You’ve always wanted to be an archivist?");
		spy.say("Oh, yes, sir.");
		spy.say("Since I was little I always keep everything in its own place.");
		spy.say("You, sir?");
		wait(0.5);
		james.say("Played a lot of “Age of Empires” when i was a child.");
		james.say("This made me try a career as an historian.");
		james.say("Couldn’t find work; ended up here, instead.");
		wait(0.5);
		spy.say("OK.");
	      });
	  case 1:
	    Co.fn({
		walkToAndFace(spy);
		james.say("...");
		james.say("I’m sorry--I’m bad at small talk.");
		spy.say("Don’t worry, sir--I’m also bad at it.");
		wait(1);
		james.say("So... did your father ever hug you?");
		spy.say("No, sir.");
		spy.say("But you can watch him hug other fathers in his burlesque show.");
		wait(.5);
		spy.say("I can ask for tickets, if interested.");
		james.say("No, thank you.");
	      });
	  default:
	    Co.fn({
		walkToAndFace(spy);
		spy.say("Found the culprit, sir?");
		james.say("Not yet.");
	      });
	  }
	}
      });

    enqueue(intro());
    if (state.first_time_archive) {
      enqueueLast(firstIntro());
    }
  }

  function intro(): Coroutine {
    james.setPosition(200, 540);
    james.faceRight();

    Co.fn({
	bgm(Res.weasel);
	fadeIn();
      });
  }

  function firstIntro(): Coroutine {
    state.first_time_archive = false;
    spy.setPosition(550, 560);
    spy.faceTowards(cabinet);

    Co.fn({
	hideCursor();
	wait(1);
	james.say("Oi, what are you doing?");
	parallel(spy.faceTowards(james), james.walkTo(spy));
	spy.say("Looking for clues, sir.");
	james.say("Just be careful with that cabinet, erm...");
	spy.say("Leio.");
	wait(1);
	james.say("Leio?");
	spy.say("Yes. It’s a Hawaiian name.");
	spy.say("Most people call me Leo, though.");
	james.say("OK, Leo. This cabinet contains documentation of some... special projects we do around here.");
	james.say("It is best to leave it alone, for now.");
	spy.say("Yes, sir.");
	spy.walkTo(450);
      });
  }

  override function handleMouseDown(x:Float, y:Float) {
    setAction(james.walkTo(Std.int(hxd.Math.clamp(x, 200, 650))), false);
  }

  function walkToAndFace(a:Actor): Coroutine {
    return run(() -> Co.fn({
	  james.walkTo(a);
	  a.faceTowards(james);
	  james.faceTowards(a);
	}));
  }

  final james:James;
  final spy:Actor;
  final door:AtlasAnim;
  final cabinet:Actor;
}