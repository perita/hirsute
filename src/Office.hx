import h2d.Text;
import hxd.Res;
import Coroutine.Result;
import Coroutine.Co;

class Office extends Scene {
  public function new() {
    super();

    final background = new AtlasBitmap("stage_office", this);

    final text = new Text(Main.ME.font, this);
    text.setPosition(1500, 100);
    text.textColor = 0x000000;
    text.letterSpacing = 0;
    text.smooth = true;
    text.textAlign = Align.Center;
    text.text = "Bureau of Moustaches and Hairdos";
    text.rotation = hxd.Math.degToRad(3);

    boss = new Boss(this);
    boss.setPosition(1500, 530);

    kachiko = new Actor("kachiko", this);
    kachiko.setPosition(900, 530);

    rae = new Actor("rae", this);
    rae.walkToPoint = 100;
    rae.setPosition(335, 560);
    rae.faceRight();

    james = new James(this);
    james.setPosition(1800, 550);

    addHotspot(1860, 220, 85, 330, "To archive");
    whenClick(() -> {
	Co.fn({
	    james.walkTo(1820);
	    james.faceRight();
	    fadeOut();
	    playAndWait(Res.open);
	    playAndWait(Res.close);
	    shiftScene(new Archive());
	  });
      });

    addHotspot(boss, "Big (Mustachioed) Boss");
    whenClick(() -> {
	if (state.has_receipt) {
	  final offset = cameraOffset;
	  Co.fn({
	      walkToAndFace(boss);
	      james.say("I’m sorry, sir.");
	      james.say("But I must do this, for your own good.");
	      boss.say("What do you mean, James?");
	      wait(0.5);
	      bgm(null);
	      parallel(boss.makeBald(), james.playAnim("reveal"), kachiko.faceTowards(boss));
	      wait(1);
	      play(Res.gasp);
	      lerpOffset(offset + 900, 1, 0);
	      boss.playStand();
	      james.playStand();
	      lerpOffset(offset, 0.5, 0);
	      wait(1.5);
	      bgm(Res.deadly);
	      boss.say("James, my boy!");
	      boss.say("What have you done!");
	      james.say("I’ve liberated you.");
	      boss.say("Liberated? My reputation is ruined!");
	      james.say("On the contrary--there’s no greater honor than to be...");
	      james.shine();
	      james.say("Bald.");
	      wait(2);
	      boss.say("You’re fired.");
	      parallel(fadeOut(3), fadeOff(Res.deadly, 3));
	      shiftScene(new Outro());
	    });
	} else {
	  switch(state.talked_boss++) {
	  case 0:
	    Co.fn({
		walkToAndFace(boss);
		boss.say("James, my boy.");
		boss.say("How’s the new intern doing?");
		james.say("It’s too soon to say, sir.");
		boss.say("You can’t go wrong with a man with buns like his. Have you seen them?");
		wait(.5);
		james.say("Yes, he has good hair.");
		wait(1);
		boss.say("Yes.");
		boss.say("That too.");
	      });
	  case 1:
	    Co.fn({
		walkToAndFace(boss);
		boss.say("James, my boy.");
		boss.say("Are you still learning to play the organ?");
		james.say("Yes, sir; every other evening.");
		boss.say("Good, good.");
		wait(.5);
		boss.say("I’ve heard women like men with huge organs. Is that true?");
		wait(1);
		james.say("I wouldn’t know, sir.");
		boss.say("That’s because you are always so tense, boy.");
		boss.say("Let your hair down once in a while.");
		wait(1);
		boss.say("As it were.");
	      });
	  default:
	    Co.fn({
		walkToAndFace(boss);
		boss.say("James, my boy.");
		boss.say("Don’t you see I’m busy?");
		boss.say("Watching everyone’s working isn’t as easy as it looks.");
	      });
	  }
	}
      });

    kachikoHs = addHotspot(kachiko, "Katherine");
    whenClick(() -> {
	switch (state.talked_kachiko++) {
	case 0:
	  Co.fn({
	      walkToAndFace(kachiko);
	      james.say("Hello Katherine.");
	      kachiko.say("*sigh*");
	      kachiko.say("How many times do I have to tell you? Call me “Kitty”!");
	      kachiko.say("C’mon, start again.");
	      kachikoHs.rename("Hairy, um, Kitty");
	      wait(0.5);
	      james.say("Hello... Kitty?");
	      kachiko.say("Hello Jimmy!");
	      kachiko.say("See? Much better!");
	      kachiko.say("What’s up?");
	      james.say("This’ll sound strange, but have you seen any suspicious character roaming about?");
	      kachiko.say("Su... suspicious character?");
	      kachiko.say("No, no, no!");
	      kachiko.say("Only my fellow human coworkers.");
	      kachiko.say("And me, of course, a human too.");
	      kachiko.say("Nothing out of the ordinary.");
	      james.say("Um.");
	      wait(0.5);
	      kachiko.say("Except...");
	      james.say("What?");
	      kachiko.say("Early I was taking my break.");
	      kachiko.say("I was having my favourite breakfast, you know.");
	      kachiko.say("Human food!");
	      kachiko.say("But there was this guy eating a really, really smelly... cheese?");
	      james.say("Smelly cheese? Was he eating it with a baguette?");
	      kachiko.say("I don’t know.... Maybe.");
	      james.say("I knew it! It was HIM!");
	      james.say("Thank you Kat... Kitty.");
	      kachiko.say("Don’ mention it.");
	    });
	case 1:
	  Co.fn({
	      walkToAndFace(kachiko);
	      james.say("Say... why “Kitty”?");
	      kachiko.say("Why, it’s short for Katherine, you silly!");
	      kachiko.say("It’s a pet name from when I was little, and it stuck.");
	      wait(1);
	      kachiko.say("Not that I’m a pet--I’m human, same as everyone else here.");
	      james.say("’Course.");
	    });
	case 2:
	  Co.fn({
	      walkToAndFace(kachiko);
	      kachiko.say("Did you know? I’ve always envied you a little.");
	      james.say("Me? Why?");
	      kachiko.say("Always hurrying to and fro the office, doing something.");
	      kachiko.say("And, yet, not a single hair out of place!");
	      wait(1);
	      kachiko.say("Astounding.");
	    });
	case 3:
	  Co.fn({
	      walkToAndFace(kachiko);
	      kachiko.say("I don’t know how can you do your job.");
	      kachiko.say("It seems so... involved.");
	      james.say("It’s not that complicated.");
	      kachiko.say("If I had to look for something in that archive, I would be tearing my hair out.");
	      wait(1);
	      kachiko.say("Oh!");
	      kachiko.say("Is that what you did?");
	    });
	default:
	  Co.fn({
	      walkToAndFace(kachiko);
	      kachiko.say("Meow.");
	      james.say("What?");
	      kachiko.say("Nothing!");
	    });
	}
      });

    addHotspot(rae, "Rae");
    whenClick(() -> {
	if (state.has_quill) {
	  if (state.has_receipt) {
	    Co.fn({
		walkToAndFace(rae);
		james.say("Better not disturb her any more.");
	      });
	  } else {
	    state.has_receipt = true;
	    Co.fn({
		walkToAndFace(rae);
		james.say("Here, your “digital quill”.");
		rae.say("Thank you a bunch.");
		rae.say("Wait a moment while I sign this.");
		wait(2);
		rae.say("Could you put this document into the archive, please?");
		rae.say("Thanks, you are lovely.");
		james.walkTo(Std.int(james.x + 100));
		james.say("Wait a minute!");
		james.say("Is that a receipt for “Hairs R Us”?");
		james.faceTowards(rae);
		james.say("Who ordered a fake mustache and a pompadour...");
		wait(.5);
		james.say("Oh, no!");
		james.say("Someone has been living a lie all this time?");
		james.say("It’s time to fix that!");
		james.say("This is more important than some bird-brained cheese-eater.");
	      });
	  }
	} else {
	  if (state.quest_for_quill) {
	    if (state.asked_rae_quill) {
	      Co.fn({
		  walkToAndFace(rae);
		  rae.say("Have you found my quill?");
		  james.say("Not yet.");
		});
	    } else {
	      state.asked_rae_quill = true;
	      Co.fn({
		  walkToAndFace(rae);
		  james.say("Pray tell, where I can find a “digital quill”?");
		  rae.say("I don’t know! Aren’t you the one dealing with certificates all day long?");
		  james.say("But I’ve never even heard of a “digital quill”.");
		  rae.say("Maybe you are getting to old to keep up with new technologies, James.");
		  james.say("But I’m only 28!");
		  rae.say("So, what? I’m 26.");
		  rae.say("Maybe it’s something that only young people use nowadays.");
		  wait(1);
		  james.say("Rae.");
		  rae.say("What?");
		  james.say("You’ve been “26” for seven years, already.");
		  rae.say("So?");
		  rae.say("Everyone grows at different rates, as they say.");
		  james.say("I don’t think this is what they mean.");
		  rae.say("Whatever. Just ask a youngster to help you!");
		});
	    }
	  } else {
	    state.quest_for_quill = true;
	    Co.fn({
		walkToAndFace(rae);
		wait(0.5);
		james.say("Huh?");
		james.say("Where is she?");
		rae.say("What do you mean “where am I?");
		rae.say("I’m right here!");
		james.say("Sorry, didn’t see you behind this....");
		rae.say("Behind this... what?");
		james.say("This... mess.");
		rae.say("“Mess”? What “mess”?");
		rae.say("I know perfectly where everything is!");
		james.say("OK.");
		wait(.25);
		rae.say("Look, my desk, my system, understood?");
		james.say("I see.");
		wait(.5);
		james.say("You haven’t had breakfast, yet, did you?");
		wait(1);
		rae.say("No.");
		james.say("Figures");
		wait(.5);
		rae.say("What do you want?");
		james.say("Forget it. I’ll return after you had something in your stomach.");
		rae.say("Yes, you better.");
		james.walkTo(Std.int(james.x + 50));
		james.faceTowards(rae);
		rae.say("Wait, James!");
		james.walkTo(rae);
		james.say("Yes?");
		rae.say("I need you to fetch me a digital quill.");
		wait(.5);
		james.say("A what?");
		rae.say("A digital quill.");
		rae.say("To digitally sign this document.");
		wait(.5);
		james.say("I don’t think that’s how it w....");
		rae.say("Look:");
		rae.say("I need to sign this document.");
		rae.say("The signature has to be digital.");
		rae.say("And signatures are, of course, made with quills.");
		rae.say("QED, I need a digital quill.");
		wait(.5);
		rae.say("And I seem to have... misplaced mine.");
		rae.say("Probably someone took it from my desk and forgot to return it back.");
		james.say("Probably.");
		rae.say("C’mon, chop-chop.");
		wait(1);
		james.say("*sigh*");
	      });
	  }
	}
	});

      if (state.first_time_office) {
	enqueue(firstIntro());
      } else {
	enqueue(intro());
      }
      }

      function walkToAndFace(a:Actor): Coroutine {
      return run(() -> Co.fn({
	    james.walkTo(a);
	    a.faceTowards(james);
	    james.faceTowards(a);
	  }));
    }

    function firstIntro() {
      state.first_time_office = false;
      boss.hide();
      james.hide();
      Co.fn({
	  disableInput();
	  bgm(null);
	  james.show();
	  boss.show();
	  parallel(fadeIn(3), fadeOff(Res.office, 3));
	  bgm(Res.weasel);
	  james.say("I need to find evidence that René’s been here.");
	});
    }

    function intro() {
      Co.fn({
	  bgm(Res.weasel);
	  parallel(fadeIn(), fadeOff(Res.office));
	});
    };

    function lerpOffset(target:Float, ?duration = 1., ?easeFactor = .25) {
      final org = cameraOffset;
      var k = 0.0;
      return (dt:Float) -> {
	k += dt;
	cameraOffset = hxd.Math.ease(org, target, Math.min(1, k / duration), easeFactor);
	return k < duration ? Continue : Halt;
      }
    }

    override function update(dt:Float) {
      super.update(dt);
      x = -Math.max(0, Math.min(2000 - 800, james.x - cameraOffset));
    }

    override function handleMouseDown(x:Float, y:Float) {
      setAction(james.walkTo(Std.int(hxd.Math.clamp(x, 510, 1820))), false);
    }

    final james:James;
    final boss:Boss;
    final kachiko:Actor;
    final rae:Actor;
    final kachikoHs:Hotspot;
    var cameraOffset = 800 / 2;
  }