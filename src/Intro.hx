import h2d.Flow;
import hxd.Res;
import Coroutine.Result;
import Coroutine.Co;

class Intro extends Scene {
  public function new() {
    super();

    final background = new AtlasBitmap("stage_archive", this);

    door = new AtlasAnim("stage_closed_door", 1, this, FlowAlign.Left, FlowAlign.Top);
    door.setPosition(51, 217);

    knock = new AtlasBitmap("stage_knock", this);
    knock.setPosition(75, 320);
    knock.hide();

    james = new James(this);

    spy = new Actor("spy", this);

    cabinet = new Actor("cabinet", this);
    cabinet.setPosition(697, 577);

    cover = new AtlasBitmap("stage_cover", this);
    cover.setPosition(-1, 205);

    office = new Office();
    office.x = -2000;
    this.addChild(office);

    addHotspot(cabinet, "Cabinet");
    whenClick(() -> {
	if (state.greeted_spy) {
	  Co.fn({
	      parallel(james.walkTo(cabinet), run(() -> Co.fn({
		      spy.walkTo(350);
		      spy.faceRight();
		    })));
	      bgm(Res.netherworld);
	      james.say("Is that a... HAIR?");
	      james.say("In MY archive?");
	      james.say("Someone’s been in here!");
	      wait(0.5);
	      spy.say("Couldn’t it be yours?");
	      james.faceTowards(spy);
	      wait(.5);
	      james.shine();
	      wait(.5);
	      james.say("No.");
	      james.say("It’s somebody else’s.");
	      james.say("And I’ll wager who.");
	      spy.say("Who?");
	      james.say("René Oiseau.");
	      wait(1);
	      spy.say("Who?");
	      james.say("René Oiseau. He’s an archivist, just like me.");
	      james.say("However, he has hair ALL over his head.");
	      spy.say("...sure.");
	      james.say("And the worst of all is that he...");
	      parallel(lerpScale(1.2, .25), pan(-800 * .1, .25));
	      james.say("Is...");
	      parallel(lerpScale(1.4, .25), pan(-800 * .2, .25));
	      james.say("French!");
	      parallel(lerpScale(1., .25), pan(0, .25));
	      wait(1);
	      spy.say("Is that a bad thing?");
	      james.say("Who wants to be French?");
	      spy.say("Um... The people of France?");
	      james.say("Exactly--Nobody!");
	      james.say("It has to be him.");
	      wait(.5);
	      spy.say("Couldn’t be somebody else?");
	      spy.say("I mean, considering where we are...");
	      pan(800);
	      wait(1.);
	      pan(0);
	      wait(0.5);
	      james.say("I’m sure it’s him. He’s been always jealous of me.");
	      james.say("I’ll show you.");
	      james.walkTo(200);
	      fadeOut();
	      play(Res.office, false);
	      playAndWait(Res.open);
	      playAndWait(Res.close);
	      shiftScene(office);
	    });
	} else {
	  Co.fn({
	      james.walkTo(cabinet);
	      james.faceTowards(cabinet);
	      james.say("This is cabinet for top-secret projects.");
	      james.say("And we it here, “hidden in plain sight”.");
	      wait(.5);
	      james.say("What could go wrong?");
	    });
	}
      });

    addHotspot(Std.int(door.x), Std.int(door.y), 85, 345, "Door");
    whenClick(() -> {
	if (state.greeted_spy) {
	  Co.fn({
	      james.say("Can not leave now.");
	      james.say("I have to see what that thing sticking out from the cabinet is.");
	    });
	} else {
	  state.greeted_spy = true;
	  Co.fn({
	      james.walkTo(spy);
	      play(Res.open);
	      door.playAnim("stage_open_door");
	      spy.show();
	      play(Res.office, true);
	      wait(.5);
	      james.say("Hi. I’m Bald.");
	      james.shine();
	      wait(.5);
	      james.say("James Bald.");
	      james.say("And I’m the head archivist.");
	      james.say("Are you the new intern?");
	      spy.say("Yes.");
	      james.say("Please, enter.");
	      spy.walkTo(450);
	      spy.faceTowards(james);
	      play(Res.close);
	      stop(Res.office);
	      door.playAnim("stage_closed_door");
	      james.walkTo(spy);
	      wait(1);
	      james.say("Welcome!");
	      james.say("You’ll like working with me.");
	      james.say("I’m pretty, laid-back, and...");
	      wait(.25);
	      bgm(null);
	      james.say("Holy smokes! What is THAT?");
	      spy.say("What?");
	      james.say("There. On the cabinet.");
	      spy.faceTowards(cabinet);
	    });
	}
      });

    addHotspot(310, 0, 180, 225, "Flourescent lighting");
    whenClick(() -> Co.fn({
	  james.say("They had to reduce the lighting’s intensity.");
	  james.say("People complained that were blinded when the light bounced off my head.");
	  wait(.5);
	  james.say("Bunch of whiners.");
	}));

    addHotspot(430, 220, 210, 265, "Archives");
    whenClick(() -> Co.fn({
	  james.say("Everything in it’s place. Neat.");
	}));

    enqueue(intro());
  }

  function intro(): Coroutine {
    james.setPosition(550, 540);
    james.faceUp();

    spy.setPosition(50, 560);
    spy.faceTowards(james);
    spy.hide();

    Co.fn({
	bgm(Res.zigzag);
	fadeIn();
	wait(1);
	james.say("I hope he doesn’t get in late.");
	wait(.25);
	play(Res.knock);
	wait(.15);
	knock.show();
	wait(.05);
	knock.hide();
	wait(.15);
	knock.show();
	wait(.05);
	knock.hide();
	wait(.15);
	knock.show();
	wait(.05);
	knock.hide();
	james.faceTowards(spy);
	james.say("Here he is, right on time.");
      });
  }

  override function handleMouseDown(x:Float, y:Float) {
    setAction(james.walkTo(Std.int(hxd.Math.clamp(x, 200, 650))), false);
  }

  function pan(to:Float, ?duration = 1.5) {
    final from = this.x;
    var t = 0.0;
    return (dt:Float) -> {
      t += dt;
      this.x = Std.int(hxd.Math.ease(from, to, Math.min(1, t / duration), 0.25));
      return t < duration ? Continue : Halt;
    }
  }

  function lerpScale(to:Float, ?duration = 1.0) {
    final from = Main.ME.s2d.scaleX;
    var t = 0.0;
    return (dt:Float) -> {
      t += dt;
      final s = hxd.Math.lerp(from, to, Math.min(1, t / duration));
      Main.ME.s2d.scaleX = s;
      Main.ME.s2d.scaleY = s;
      return t < duration ? Continue : Halt;
    }
  }

  final james:James;
  final spy:Actor;
  final cover:AtlasBitmap;
  final door:AtlasAnim;
  final office:Office;
  final cabinet:Actor;
  final knock:AtlasBitmap;
}