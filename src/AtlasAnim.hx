import h2d.Flow;

enum Face {
  Up;
  Right;
  Left;
}

class AtlasAnim extends h2d.Anim {
  public var animName:String;
  public var face(default, set): Face = Left;

  public var hotspotX (get, null):Int;
  public var hotspotY (get, null):Int;
  public var hotspotW (get, null):Int;
  public var hotspotH (get, null):Int;

  public function new(name:String, speed:Float, ?parent:h2d.Object, ?horizontalAlign = FlowAlign.Middle, ?verticalAlign = FlowAlign.Bottom) {
    super([], speed, parent);
    this.halign = horizontalAlign;
    this.valign = verticalAlign;
    this.animName = name;
    face = Left;
    replay();
    Main.ME.watchAtlas(replay);
  }

  function replay() {
    if (animName == null) {
      return;
    }
    playAnim(animName, currentFrame);
  }

  public function playAnim(name:String, atFrame = 0.0) {
    animName = name;
    final frames = getFrames(name);
    play(frames, Math.min(frames?.length ?? 0, atFrame));
    if (face == Right) {
      flipX();
    }
    return null;
  }

  function getFrames(name:String) {
    return hxd.Res.tiles.getAnim(name, halign, valign);
  }

  function flipX() {
    for (frame in frames) {
      frame?.flipX();
    }
  }

  public function hide() {
    visible = false;
    return null;
  }

  public function show() {
    visible = true;
    return null;
  }

  function set_face(newFace) {
    if (face != newFace && face != Up) {
      flipX();
    }
    return face = newFace;
  }

  function get_hotspotX() return Std.int(x - (hotspotW >> 1));
  function get_hotspotY() return Std.int(y - hotspotH);
  function get_hotspotW() return getFrame().iwidth;
  function get_hotspotH() return getFrame().iheight;

  var halign:FlowAlign;
  var valign:FlowAlign;
}