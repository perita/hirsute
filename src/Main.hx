import h2d.Bitmap;
import h2d.Font;
import h2d.Tile;
import hxd.Cursor;
import hxd.Res;
import hxd.System;
import hxd.res.Sound;

class Main extends hxd.App {
  public static var ME: Main;

  public static function main() {
    new Main();
  }

  public function new() {
    ME = this;
    super();
  }

  override function loadAssets(onLoaded:()->Void) {
#if (hl && debug)
    Res.initLocal();
#else
    Res.initEmbed();
#end
    Res.tiles.watch(reloadAtlas);
    onLoaded();
  }

  override function init() {
    s2d.scaleMode = LetterBox(800, 600, true, Center, Center);
    s2d.camera.clipViewport = true;
    engine.backgroundColor = 0xffffff;

    font = Res.AmaticSC.toSdfFont(42, SDFChannel.MultiChannel, 0.5, 4 / 24);

    final cursorTile = Res.tiles.get("stage_cursor");
    final bounds = h2d.col.IBounds.fromValues(cursorTile.ix, cursorTile.iy, cursorTile.iwidth, cursorTile.iheight);
    var cursorBitmap = new hxd.BitmapData(cursorTile.iwidth, cursorTile.iheight);
    cursorBitmap.setPixels(cursorTile.getTexture().capturePixels(bounds));
    defaultCursor = Custom(new CustomCursor([cursorBitmap], 0, cursorTile.iwidth >> 1, cursorTile.iheight >> 1));
    showCursor();

    fade = new Bitmap(Tile.fromColor(engine.backgroundColor));
    fade.width = s2d.width;
    fade.height = s2d.height;
    s2d.add(fade, 1);

    s2d.addEventListener((event) -> {
	switch (event.kind) {
	case EPush:
	  onMouseDown(event);
	default:
	}
      });

    #if debug
    state.first_time_office = false;
    state.first_time_archive = false;
    state.quest_for_quill = true;
    state.has_quill = true;
    shiftScene(new Office());
    #else
    shiftScene(new Intro());
    #end
  }

  function reloadAtlas() {
    @:privateAccess Res.tiles.contents = null;
    for (callback in reloadAtlasCallbacks) {
      callback();
    }
  }

  public function watchAtlas(callback:()->Void) {
    reloadAtlasCallbacks.push(callback);
  }

  public function showCursor() {
    sevents.defaultCursor = defaultCursor;
  }

  public function hideCursor() {
    sevents.defaultCursor = Hide;
  }

  public function shiftScene(s:Scene) {
    scene?.remove();
    scene = s;
    s2d.add(scene, 0);
    setMouseListener(scene);
  }

  override function update(dt:Float) {
    scene?.update(dt);
  }

  function onMouseDown(e:hxd.Event) {
    if (mouseListeners.length > 0) {
      mouseListeners[mouseListeners.length - 1].onMouseDown(e);
    }
  }

  public function setMouseListener(listener:MouseListener) {
    while(mouseListeners.length > 0) {
      popMouseListener();
    }
    pushMouseListener(listener);
  }

  public function pushMouseListener(listener:MouseListener) {
    mouseListeners.push(listener);
  }

  public function popMouseListener() {
    mouseListeners.pop();
  }

  public function bgm(song:Sound) {
    if (song == this.song) {
      return;
    }
    if (this.song != null) {
      this.song.stop();
    }
    this.song = song;
    if (this.song != null) {
      this.song.play(true);
    }
  }

  public var font:Font;
  public var fade:Bitmap;
  public var defaultCursor:Cursor;
  public final state = new State();
  final reloadAtlasCallbacks:Array<()->Void> = [];
  final mouseListeners:Array<MouseListener> = [];
  var scene:Scene = null;
  var song:Sound = null;
}