import hxd.Res;

class James extends Actor {
  public function new(?parent:h2d.Object) {
    super("james", parent);
  }

  public function shine(): Coroutine {
    hxd.Res.shine.play();
    return waitAnim("shine");
  }
}