import h2d.Object;
import h2d.Text;
import hxd.Res;

class Speech extends Object implements MouseListener {
  public var done = false;
  
  public function new(y:Float, line:String, actor:Object) {
    super(Main.ME.s2d);

    final t = actor.localToGlobal();
    t.y += y * Main.ME.s2d.scaleY;
    this.globalToLocal(t);
    this.y = t.y;
    this.x = t.x;

    final tail = new h2d.Bitmap(Res.tiles.get("speech_tail"), this);
    tail.color.setColor(0xff000000);
    tail.setPosition(-tail.tile.width / 2, -tail.tile.height);
    
    final bubble = new h2d.ScaleGrid(Res.tiles.get("speech_bubble"), 32, 32, 32, 32, tail);

    final text = new Text(Main.ME.font, bubble);
    text.maxWidth = 400;
    text.setPosition(32, 0);
    text.textColor = 0xffffff;
    text.letterSpacing = 0;
    text.smooth = true;
    text.textAlign = Align.MultilineCenter;
    text.text = line;

    bubble.color.load(tail.color);
    bubble.width = text.textWidth + 64;
    bubble.height = Math.max(64, text.textHeight + 12);
    text.y = bubble.height / 2 - text.textHeight / 2 - 6;
    bubble.setPosition(-tail.x - bubble.width / 2, -bubble.height);

    final p = bubble.localToGlobal();
    if (p.x < 0) {
      bubble.x -= p.x;
    } else if ((p.x + bubble.width) > 800) {
      bubble.x -= p.x + bubble.width - 800;
    }
    if (p.y < 0) {
      tail.y -= p.y;
    }

    Main.ME.pushMouseListener(this);
  }

  public function onMouseDown(e:hxd.Event) {
    done = true;
  }

  public function finish() {
    Main.ME.popMouseListener();
    remove();
  }
}