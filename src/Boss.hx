class Boss extends Actor {
  
  public function new(?parent:h2d.Object) {
    super("boss", parent);
    walkToPoint = -120;
  }

  public function makeBald() {
    this.name = "baldboss";
    playAnim("reveal");
    return null;
  }
}