@:build(StateMacro.build())
class State {
  public function new() {
  }

  var has_receipt = false;
  var greeted_spy = false;
  var first_time_office = true;
  var talked_boss = 0;
  var talked_kachiko = 0;
  var first_time_archive = true;
  var commented_buns = false;
  var talked_leo = 0;
  var has_quill = false;
  var quest_for_quill = false;
  var asked_boss_quill = false;
  var asked_kachiko_quill = false;
  var asked_rae_quill = false;
}