import h2d.Object;
import hxd.System;
import hxd.res.Sound;

class Scene extends Object implements MouseListener {

  public function update(dt:Float) {
    if (queue.length > 0) {
      final c = queue[queue.length - 1];
      switch (c(dt)) {
      case Continue: // nothing to do
      case Call(o):
	if (o != null) {
	  queue.push(o);
	}
      case TailCall(o):
	queue.pop();
	if (o == null) {
	  maybeEnableInput();
	} else {
	  queue.push(o);
	}
      case Halt:
	queue.pop();
	maybeEnableInput();
      }
    }
  }

  function enqueue(c:Coroutine, ?disableInput = true) {
    queue.push(c);
    if (!inputDisabled && disableInput) {
      inputDisabled = true;
    }
  }

  function enqueueLast(c:Coroutine, ?disableInput = true) {
    queue.unshift(c);
    if (!inputDisabled && disableInput) {
      inputDisabled = true;
    }
  }

  function disableInput() {
    return (dt:Float) -> {
      inputDisabled = true;
      return Coroutine.Result.Halt;
    };
  }

  function setAction(c:Coroutine, ?disableInput = true) {
    while (queue.length > 0) {
      queue.pop();
    }
    enqueue(c, disableInput);
  }

  function maybeEnableInput() {
    if (inputDisabled && queue.length == 0) {
      inputDisabled = false;
    }
  }

  overload extern inline public function addHotspot(a:AtlasAnim, name:String, ?parent:Object = this) {
    return addHotspot(a.hotspotX, a.hotspotY, a.hotspotW, a.hotspotH, name, parent);
  }

  overload extern inline public function addHotspot(x:Int, y:Int, w:Int, h:Int, name:String, ?parent:Object = this) {
    final hs = new Hotspot(w, h, parent);
    hs.setPosition(x, y);
    hs.name = name;
    hs.cursor = Main.ME.defaultCursor;
    hs.visible = !inputDisabled;
    hotspots.push(hs);
    lastHotspot = hs;
    return hs;
  }

  function whenClick(f:Void->Coroutine) {
    if (lastHotspot == null) return;
    lastHotspot.onClick = (_) -> {
      setAction(f());
    }
  }

  function run(f:Void->Coroutine):Coroutine {
    final q = [f()];
    return (dt:Float) -> {
      final c = q[q.length - 1];
      switch(c(dt)) {
      case Continue: // nothing to do
      case Call(o):
	if (o != null) {
	  q.push(o);
	}
      case TailCall(o):
	q.pop();
	if (o != null) {
	  q.push(o);
	}
      case Halt:
	q.pop();
      }
      return q.length == 0 ? Halt : Continue;
    }
  }

  function parallel(...actions:Coroutine):Coroutine {
    final all = actions.toArray();
    return (dt:Float) -> {
      var result:Coroutine.Result = Halt;
      for (i in 0...all.length) {
	final c = all[i];
	if (c == null) continue;
	switch (c(dt)) {
	case Continue:
	  result = Continue;
	case Halt:
	  all[i] = null;
	case Call(o):
	  return Call(o);
	case TailCall(o):
	  all[i] = null;
	  return TailCall(o);
	}
      }
      return result;
    }
  }

  function wait(secs:Float):Coroutine {
    return (dt:Float) -> {
      secs -= dt;
      return secs <= 0 ? Halt : Continue;
    }
  }

  function play(snd:Sound, ?loop = false, ?volume = 1.):Coroutine {
    snd.play(loop, volume);
    return null;
  }

  function stop(snd:Sound) {
    snd.stop();
    return null;
  }

  function fadeOff(snd:Sound, ?duration:Float = 1.0):Coroutine {
    @:privateAccess
      final channel = snd.channel;
    if (channel == null) {
      return null;
    }
    final from = channel.volume;
    var t = 0.0;
    return (dt:Float) -> {
      t += dt;
      channel.volume = hxd.Math.lerp(from, 0, Math.min(1, t / duration));
      return t < duration ? Continue : Halt;
    }
  }

  function playAndWait(snd:Sound):Coroutine {
    var done = false;
    final ch = snd.play(false);
    ch.onEnd = () -> { done = true; }
    return (dt:Float) -> {
      return done ? Halt : Continue;
    }
  }

  function fadeIn(?duration = 1.0) return fade(1, 0, duration);
  function fadeOut(?duration = 1.0) return fade(0, 1, duration);

  function fade(from:Float, to:Float, ?duration = 1.0):Coroutine {
    var t = 0.0;
    Main.ME.fade.alpha = from;
    return (dt:Float) -> {
      t += dt;
      Main.ME.fade.alpha = hxd.Math.lerp(from, to, Math.min(1, t / duration));
      return t < duration ? Continue : Halt;
    }
  }

  function shiftScene(scene:Scene) {
    Main.ME.shiftScene(scene);
    return null;
  }

  function set_inputDisabled(d:Bool):Bool {
    if (inputDisabled == d) {
      return d;
    }
    for (hs in hotspots) {
      hs.visible = !d;
    }
    if (d) {
      hideCursor();
    } else {
      Main.ME.showCursor();
    }

    return inputDisabled = d;
  }

  function hideCursor() {
    Main.ME.hideCursor();
    return null;
  }

  public function onMouseDown(e:hxd.Event) {
    if (inputDisabled) {
      return;
    }
    tmp.set(e.relX, e.relY);
    globalToLocal(tmp);
    handleMouseDown(tmp.x, tmp.y);
  }

  function handleMouseDown(x:Float, y:Float) {
  }

  public function bgm(song:Sound) {
    Main.ME.bgm(song);
    return null;
  }

  function get_state() return Main.ME.state;

  final queue:Array<Coroutine> = [];
  final hotspots:Array<Hotspot> = [];
  var lastHotspot:Hotspot;
  var inputDisabled (default, set):Bool;
  var state(get, null):State;
  final tmp = new h2d.col.Point();
}