import h2d.Bitmap;
import h2d.Text;
import h2d.Tile;
import hxd.Event;

class Hotspot extends h2d.Interactive {
  public function new(width:Float, height:Float, ?parent:h2d.Object) {
    super(width, height, parent);
  }

  override function onOver(e:Event) {
    if (label == null) {
      final text = new Text(Main.ME.font);
      text.textColor = 0x000000;
      text.text = name;
      text.setPosition(10, 0);
      text.smooth = true;
  
      final background = Tile.fromColor(0xffffff);
      label = new Bitmap(background);
      label.width = Std.int(text.textWidth) + 20;
      label.height = Std.int(text.textHeight) + 10;
      label.addChild(text);

      Main.ME.s2d.add(label, 2);
    }
    moveName(e.relX, e.relY);
  }

  override function onMove(e:Event) {
    moveName(e.relX, e.relY);
  }

  function moveName(x:Float, y:Float) {
    if (label != null) {
      label.visible = true;
      tmp.set(x - label.width / 2, y - label.height - 30);
      this.localToGlobal(tmp);
      if (tmp.y < 0) {
	tmp.set(x - label.width / 2, y + 30);
	this.localToGlobal(tmp);
      }
      label.x = Std.int(tmp.x);
      label.y = Std.int(tmp.y);
    }
  }

  override function onOut(e:Event) {
    if (label != null) {
      label.visible = false;
    }
  }

  override function onRemove() {
    super.onRemove();
    label.remove();
    label = null;
  }

  override function set_visible(b) {
    super.set_visible(b);
    if (label != null && !b) {
      label.visible = false;
    }
    return b;
  }

  public function rename(n:String) {
    if (label != null) {
      label.remove();
      label = null;
    }
    name = n;
    return null;
  }

  var label:Bitmap;
  final tmp = new h2d.col.Point();
}
