import h2d.Text;
import h2d.Font;
import hxd.Res;
import Coroutine.Result;
import Coroutine.Co;

class Outro extends Scene {
  public function new() {
    super();

    background = new AtlasBitmap("spy_end", this);
    
    spy = new Actor("spy", this);

    final font = Res.AmaticSC.toSdfFont(84, SDFChannel.MultiChannel, 0.5, 4 / 48);
    text = new Text(font);
    text.maxWidth = 400;
    text.textColor = 0x000000;
    text.letterSpacing = 0;
    text.smooth = true;
    text.text = "The End...?";
    text.alpha = 0;
    text.setPosition(800 / 2 - text.textWidth / 2, 600 / 2 - text.textHeight / 2);
    Main.ME.s2d.add(text, 2);

    queue.push(outro());
  }

  function outro(): Coroutine {
    spy.visible = false;
    spy.setPosition(500, 400);
    Co.fn({
	disableInput();
	bgm(null);
	wait(1);
	playAndWait(Res.ringback);
	wait(1);
	play(Res.ringback);
	fadeIn(2);
	bgm(Res.crypto);
	spy.say("It’s me.");
	spy.say("The dirty deed has been done.");
	wait(2);
	spy.say("No, sir, no problems.");
	spy.say("We used hair as distraction, and sent him into a wild-goose chase.");
	spy.say("And ultimately to his downfall.");
	wait(1);
	spy.say("Yes, he blames the Frenchman.");
	wait(1);
	spy.say("No, sir, not The Merovingian--the OTHER Frenchman.");
	wait(2);
	spy.say("Yes, sir, I have it in my hands.");
	pan(2);
	wait(.5);
	spy.say("All the documentation for the “Golden Conditioner” Project.");
	spy.say("The project that bald bastard was keeping away from us with his misplaced ideals.");
	wait(1);
	spy.say("Thank you, sir. Good-bye.");
	fadeOut(2);
	showText(1);
      });
  }

  function pan(duration:Float) {
    var t = 0.0;
    return (dt:Float) -> {
      t += dt;
      background.y = Std.int(hxd.Math.ease(0, 600 - 787, Math.min(1, t / duration), 0.25));
      return t < duration ? Continue : Halt;
    }
  }

  function showText(duration:Float) {
    var t = 0.0;
    return (dt:Float) -> {
      t += dt;
      text.alpha = hxd.Math.lerp(0.0, 1.0, Math.min(1, t / duration));
      return t < duration ? Continue : Halt;
    }
  }


  final spy:Actor;
  final background:AtlasBitmap;
  final text:Text;
}