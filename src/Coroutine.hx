import haxe.macro.Expr;
import haxe.macro.Context;

typedef Coroutine = Float->Result;

enum Result {
  Continue;
  Halt;
  Call(c:Null<Coroutine>);
  TailCall(c:Null<Coroutine>);
}


class Co {

  macro public static function fn(expr:Expr):Expr {
    var calls = switch(expr.expr) {
    case EBlock(exprs): exprs;
    default: [expr];
    }
    var cases:Array<Case> = [ for (index in 0...calls.length)
	{values: [macro $v{index}], guard: null, expr: index == calls.length - 1 ? macro { return TailCall(${calls[index]}); } : macro { s++; return Call(${calls[index]}); } } ];
    var e = { expr: ESwitch(macro $i{'s'}, cases, null), pos: Context.currentPos() };
    return macro {
      var s = 0;
      return ((dt:Float) -> {
	${e}
	return Halt;
	}:Coroutine);
    }
  }

  macro public static function call(e:Expr):Expr {
    return macro {
      s++;
      return Call(${e});
    };
  }

  macro public static function tail(e:Expr):Expr {
    return macro return TailCall(${e});
  }
}
