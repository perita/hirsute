import haxe.macro.Expr;
import haxe.macro.Context;

class StateMacro {
  public static function build():Array<Field> {
    final fields = Context.getBuildFields();
    final add = [];

    for (f in fields) {
      switch(f.kind){
      case FVar(t, e):
	f.access.push(APublic);
	final name = f.name;
	var setFun:Function = {
	args:[{name: "v"}],
	expr: macro {
	    this.$name = v;
	    return null;
	  }
	};
	var set:Field = {
	name: "set_" + f.name,
	access: [APublic],
	kind: FFun(setFun),
	pos: f.pos,
	};
	add.push(set);
      default:
      }
    }

    for (a in add) {
      fields.push(a);
    }
    return fields;
  }
}

