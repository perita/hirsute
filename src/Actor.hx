import h2d.Object;
import Coroutine.Result;

class Actor extends AtlasAnim {
  public var walkSpeed = 150;
  public var textPos = -300;
  public var walkToPoint:Null<Int>;

  public function new (name:String, ?parent:Object) {
    super(null, 7, parent);
    this.name = name;
    playAnim("stand");
  }

  override function getFrames(anim:String) {
    return super.getFrames(name + "_" + anim);
  }

  public function faceRight() {
    face = Right;
    if (animName == "stand_u") {
      playStand();
    }
    return null;
  }

  public function faceLeft() {
    face = Left;
    if (animName == "stand_u") {
      playStand();
    }
    return null;
  }

  public function faceTowards(o:Object) {
    return o.x < x ? faceLeft() : faceRight();
  }

  public function faceUp() {
    face = Up;
    if (animName == "stand") {
      playStand();
    }
    return null;
  }

  public function playStand() {
    return playAnim(face == Up ? "stand_u" : "stand");
  }

  public function playTalk() {
    return playAnim(face == Up ? "talk_u" : "talk");
  }

  overload extern inline public function walkTo(x: Int): Coroutine {
    playAnim("walk");
    var fx = this.x;
    var speed = walkSpeed;
    var cmp:Void->Bool = null;
    if (this.x > x) {
      faceLeft();
      speed = -speed;
      cmp = () -> this.x <= x;
    } else {
      faceRight();
      cmp = () -> this.x >= x;
    }
    return (dt:Float) -> {
      if (cmp()) {
	playStand();
	return Halt;
      }
      fx += speed * dt;
      this.x = Std.int(fx);
      return Continue;
    }
  }

  overload extern inline public function walkTo(a:Actor):Coroutine {
    if (a.walkToPoint == null) {
      return walkTo(cast(a, Object));
    }
    return walkTo(Std.int(a.x + a.walkToPoint));
  }

  overload extern inline public function walkTo(o:Object):Coroutine {
    if (o.x < x) {
      return walkTo(Std.int(o.x + 200));
    } else {
      return walkTo(Std.int(o.x - 200));
    }
  }

  public function say(what:String): Coroutine {
    final speech = new Speech(textPos, what, this);
    playTalk();
    var time = 0.25;
    return (dt:Float) -> {
      if (time > 0) {
	time -= dt;
	speech.done = false;
	return Continue;
      }
      if (!speech.done) {
	return Continue;
      }
      speech.finish();
      playStand();
      return Halt;
    }
  }

  public function waitAnim(anim:String): Coroutine {
    loop = false;
    final prev = onAnimEnd;
    playAnim(anim);
    var done = false;
    onAnimEnd = () -> done = true;
    return (dt:Float) -> {
      if (!done) {
	return Continue;
      }
      onAnimEnd = prev;
      loop = true;
      playStand();
      return Halt;
    }
  }
}